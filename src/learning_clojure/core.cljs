(ns learning-clojure.core
  (:require))

(enable-console-print!)

(def total-width (* 16 4))

(def total-height (* 9 4))

(def log-height (* 2 (/ total-height 3)))

(def command-height (/ total-height 3))

(def max-scrollback 128)

(def default-frame-length-ms 1000)

(def default-frame-color "#BAF2FF")

(def global-frame-length-ms 16)

(defn append-log [logs log]
  (let [logs (conj logs log)]
    (if (> (count logs) max-scrollback)
      (vec (rest logs))
      logs)))

(defn game-state-append-logs [game-state & logs]
  (assoc game-state :logs (reduce append-log (:logs game-state) logs)))

(defn game-state-clear-commands 
  ([game-state]
   (assoc game-state :commands []))
  ([game-state pred]
   (assoc game-state :commands (remove pred (:commands game-state)))))

(defn game-state-clear-commands-with-function [game-state function]
  (game-state-clear-commands game-state (fn [command] (= function (:function command)))))

(defn game-state-append-commands [game-state & commands]
  (assoc game-state :commands (vec (concat commands (:commands game-state)))))

(defn hungry [game-state]
  (-> game-state
      (game-state-append-logs [{:text "A capsule beside you opens at your touch. The thick liquid within is hot, and smells like food. You drink deeply."}])
      (game-state-clear-commands-with-function hungry)
      ))

(defn fix-power [game-state]
  (-> game-state
      (game-state-append-logs [{:text "After a moment's thought, you summon the strength to give the finger-pad a solid thump."}]
                              [{:text "The bassy hum sputters into an unbearable whine, then quickly fades to an almost-calming white noise."}]
                              [{:text "The lights are on now."}])
      (game-state-clear-commands-with-function fix-power)))

(defn examine-ship [game-state]
  (-> game-state
      (game-state-append-logs [{:text "Crates and capsules are strewn around the cramped compartment, all covered in a healthy layer of the same faintly-sparkling dust that is floating in the air."}]
                              [{:text "You seem to be laying on the floor... and everything hurts."}])
      (game-state-clear-commands)
      (game-state-append-commands {:text "You're reaaaally hungry." :function hungry}
                                  {:text "Fix the flickering power." :function fix-power})))

(defn press-keypad [game-state]
  (-> game-state
      (game-state-append-logs [{:text "A deep hum fills the dark space around you."}]
                              [{:text "Moving your arm feels as though you are reviving it from death. After a tenuous moment, you outstretch to the illuminated panel."}]
                              [{:text "A light begins to flicker, illuminating the thick dust suspended in the dead air around you." :color "#334444" :frame-length-ms 2000}
                               {:text "A light begins to flicker, illuminating the thick dust suspended in the dead air around you." :color "#CBF3FF" :frame-length-ms 200}
                               {:text "A light begins to flicker, illuminating the thick dust suspended in the dead air around you." :color "#A9E1EE" :frame-length-ms 600}
                               {:text "A light begins to flicker, illuminating the thick dust suspended in the dead air around you." :color "#334444" :frame-length-ms 200}
                               {:text "A light begins to flicker, illuminating the thick dust suspended in the dead air around you." :color "#A9E1EE" :frame-length-ms 800}]
                              [{:text "   .     .              .            .                     ."}
                               {:text "   .           .        .                                  ."}
                               {:text "               .        .                        .         ."}
                               {:text "               .                     .           .          "}
                               {:text "   .                                 .           .          "}]
                              [{:text "            .        .                        .         .   "}
                               {:text "            .                     .           .             "}
                               {:text ".                                 .           .             "}
                               {:text ".     .              .            .                     .   "}
                               {:text ".           .        .                                  .   "}]
                              [{:text "       .                                 .           .      "}
                               {:text "       .     .              .            .                  "}
                               {:text "       .           .        .                               "}
                               {:text "                   .        .                        .      "}
                               {:text "                   .                     .           .      "}])
      (game-state-clear-commands)
      (game-state-append-commands {:text "Examine your surroundings." :function examine-ship})))

(defn init-display []
  (let
   [main-div (.getElementById js/document "app")
    log-div (.createElement js/document "div")
    log-div-style (str
                   "color: #BAF2FF;"
                   "background-color: #5D7980;"
                   "overflow-y: scroll;"
                   "overflow-x: hidden;"
                   "height: " log-height "em;"
                   "width: " total-width "em;"
                   "padding: 1em;")
    commands-div (.createElement js/document "div")
    commands-div-style (str
                        "color: #8CB6BF;"
                        "background-color: #2F3D40;"
                        "overflow-y: scroll;"
                        "overflow-x: hidden;"
                        "height: " command-height "em;"
                        "width: " total-width "em;"
                        "padding: 1em;")]
    (set! (.-innerHTML main-div) "")
    (set! (.-innerHTML log-div) "Welcome...")
    (set! (.-innerHTML commands-div) "Oof!")
    (.setAttribute log-div "style" log-div-style)
    (.setAttribute commands-div "style" commands-div-style)
    (.appendChild main-div log-div)
    (.appendChild main-div commands-div)
    {:logs log-div :commands commands-div}))

(defn log-frame-text [log]
  (let [millis (.now js/Date)
        anim-length (reduce (fn [acc log-frame] (+ acc (:frame-length-ms log-frame default-frame-length-ms))) 0 log)
        millis-mod (mod millis anim-length)]
    (loop [acc 0 n 0]
      (if (<= acc millis-mod)
        (recur (+ acc (:frame-length-ms (nth log n) default-frame-length-ms)) (inc n))
        (let [frame (nth log (- n 1))]
          (str "<div style=\"margin-top: 0.25em; white-space: pre-wrap; color:" (:color frame default-frame-color) ";\"" ">" (:text frame) "</div>"))))))

(defn init-game-state []
  {:display (init-display)
   :logs (-> [] (append-log [{:text "You slowly awaken.." :color "#98D0DD"}
                             {:text "You slowly awaken..." :color "#A9E1EE"}
                             {:text "You slowly awaken...." :color "#BAF2FF"}
                             {:text "You slowly awaken....." :color "#CBF3FF"}])
             (append-log [{:text "Standing out from the inky darkness, a pulsating light emerges from a small panel before you."}]))
   :commands [{:text "Press the dimly glowing finger-pad." :function press-keypad}]})

(defn game-state-update
  "Updates the game state using an update function. Returns the updated game state. The update function must take a game state and return a game state."
  [game-state update-function]
  (update-function game-state)
  ;; todo check for null return from update function and print a warning
  )

(defn game-state-display-logs
  "Updates the logs panel using the game state. Returns nothing."
  [game-state]
  (let [logs-div (:logs (:display game-state))]
    (set!
     (.-innerHTML logs-div)
     (str (clojure.string/join "<br/>" (map log-frame-text (:logs game-state))) "<br/><br/>"))))

(defn game-state-set-command-input
  "Displays a command and attaches it's event listener."
  [game-state command]
  (let
   [command-div (.createElement js/document "div")
    commands-div (:commands (:display game-state))]
    (set! (.-innerHTML command-div) (str "<u>" (:text command) "</u>"))
    (.setAttribute command-div "class" "command")
    (.appendChild commands-div command-div)
    {:element command-div :function (:function command)}))

(defn game-state-set-command-inputs
  "Displays the commands and attaches their event listeners."
  [game-state]
  (let [commands-div (:commands (:display game-state))]
    (set! (.-innerHTML commands-div) "")
    (map (fn [command] (game-state-set-command-input game-state command)) (:commands game-state))))

(defn game-state-display
  "Displays the game state, so that the player may interact with it."
  [game-state]
  (when (contains? game-state :animation)
    (.clearInterval js/window (:animation game-state)))
  (game-state-display-logs game-state)
  (set!
   (.-scrollTop (:logs (:display game-state)))
   (.-scrollHeight (:logs (:display game-state))))
  (let [game-state (assoc game-state :animation (.setInterval js/window (fn [] (game-state-display-logs game-state)) global-frame-length-ms))]
    (doseq
     [command (game-state-set-command-inputs game-state)]
      (.addEventListener
       (:element command) "mousedown"
       (fn [] (game-state-display (game-state-update game-state (:function command))))))))

(defn main []
  (let [game-state (init-game-state)]
    (game-state-display game-state)))

(main)

;; what can happen when you select a command:
;; - a function is called which returns a new game state!